from django import forms


class BasketAddProductForm(forms.Form):
    qty = forms.IntegerField(min_value=1)