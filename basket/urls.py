from django.urls import path
from . import views

app_name = 'basket'

urlpatterns = [
    path('summary/', views.basket_summary, name='basket_summary'),
    path('summary/add/', views.basket_add, name='basket_add'),
    path('summary/delete/', views.basket_delete, name='basket_delete'),
    path('summary/update/', views.basket_update, name='basket_update'),
]