from django.contrib.auth.models import User
from django.test import TestCase
from django.urls import reverse

from store.models import Product, Category


class TestCategory(TestCase):
    def setUp(self) -> None:
        self.category = Category.objects.create(name='category', slug='category')

    def test_category_model_input(self):
        """Test data input to category field"""
        self.assertTrue(isinstance(self.category, Category))

    def test_category_model_return(self):
        """Test returning attributes from Category model"""
        self.assertEqual(str(self.category), 'category')

    def test_category_url(self):
        """Test response status category url"""
        sample = self.category
        response = self.client.post(reverse('store:category_list', args=[sample.slug]))
        self.assertEqual(response.status_code, 200)


class TestProduct(TestCase):
    def setUp(self) -> None:
        User.objects.create(username='admin')
        Category.objects.create(name='category', slug='category')
        self.prod1 = Product.products.create(category_id=1, title='title', created_by_id=1, slug='title', price='19.99')
        self.prod2 = Product.products.create(category_id=1, title='title2', created_by_id=1, slug='title2', price='19',
                                             is_active=False)

    def test_product_model_input(self):
        """Test data input to product field"""
        self.assertTrue(isinstance(self.prod1, Product))

    def test_product_model_return(self):
        """Test returning attributes from Product model"""
        self.assertEqual(self.prod1.title, 'title')
        self.assertEqual(self.prod2.title, 'title2')

    def test_product_url(self):
        """Test response status product url"""
        sample = self.prod1
        response = self.client.post(reverse('store:product_detail', args=[sample.slug]))
        self.assertEqual(response.status_code, 200)

    def test_product_manager_custom(self):
        """Test product manager returns only is_active product"""
        self.assertEqual(Product.products.all().count(), 1)
