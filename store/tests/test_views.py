from importlib import import_module

from django.conf import settings
from django.contrib.auth.models import User
from django.http import HttpRequest
from django.urls import reverse
from django.test import TestCase, Client

from store.models import Product, Category
from store.views import products_all


class TestViewResponse(TestCase):

    def setUp(self) -> None:
        self.clt = Client()
        User.objects.create(username='admin')
        Category.objects.create(name='category', slug='category')
        self.prod = Product.products.create(category_id=1, title='title', created_by_id=1, slug='title', price='19.99')

    def test_url_allowed_hosts(self):
        """Test allowed hosts"""
        response = self.clt.get('/')
        self.assertEqual(response.status_code, 200)

    def test_product_detail_url(self):
        """Test product detail url"""
        response = self.clt.get(reverse('store:product_detail', args=['title']))
        self.assertEqual(response.status_code, 200)

    def test_category_detail_url(self):
        """Test category detail url"""
        response = self.clt.get(reverse('store:category_list', args=['category']))
        self.assertEqual(response.status_code, 200)

    def test_homepage_html(self):
        """Test homepage html"""
        request = HttpRequest()
        engine = import_module(settings.SESSION_ENGINE)
        request.session = engine.SessionStore()
        response = products_all(request)
        html = response.content.decode('utf8')
        self.assertIn('<title> MyStore </title>', html)
        self.assertTrue(html.startswith('\n<!DOCTYPE html>\n'))
        self.assertEqual(response.status_code, 200)
